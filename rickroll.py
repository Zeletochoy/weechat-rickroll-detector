#!/usr/bin/env python
# encoding: utf-8

import weechat
from apiclient.discovery import build
import urlparse


def id_from_url(url):
    if not url.startswith("http"):
        url = "http://" + url
    query = urlparse.urlparse(url)
    if query.hostname == 'youtu.be':
        return query.path[1:]
    if query.hostname in ('www.youtube.com', 'youtube.com'):
        if query.path == '/watch':
            p = urlparse.parse_qs(query.query)
            return p['v'][0]
        if query.path[:7] == '/embed/':
            return query.path.split('/')[2]
        if query.path[:3] == '/v/':
            return query.path.split('/')[2]
    return None


def yt_title(video_id):
    yt = build('youtube', 'v3',
               developerKey='CHANGEME')
    res = yt.videos().list(part="snippet", id=video_id).execute()
    return res["items"][0]["snippet"]["title"]


def defcon_level(title):
    keywords = ["rick", "astley", "rickroll",
                "never", "gonna", "give", "you", "up"]
    title = set(title.lower().split())
    n = 0
    for kw in keywords:
        if kw in title:
            n += 1
    return max(1, 5-n)


def check_link(data, buf, date, tags, displayed, hilight, prefix, msg):
    for w in msg.split():
        video_id = id_from_url(w)
        if video_id is not None:
            title = yt_title(video_id)
            defcon = defcon_level(title)
            if defcon < 5:
                weechat.prnt(buf,
                             "Rickroll level DEFCON %d (%s)" % (defcon, title))
    return weechat.WEECHAT_RC_OK


weechat.register("rickroll", "Zeletochoy", "1.0", "BeerWare",
                 "Rickroll links detector", "", "")
weechat.hook_print('', 'irc_privmsg', '', 1, 'check_link', '')
